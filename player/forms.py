from django import forms # imports form class from django
from player.models import Player

class PlayerForm(forms.ModelForm):
    class Meta:
        model = Player 
        fields = "__all__" 
